package com.entappia.ei4ovisitorprofile.datacollection;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4ovisitorprofile.constants.AppConstants;
import com.entappia.ei4ovisitorprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4ovisitorprofile.udp.client.UdpClient;
import com.entappia.ei4ovisitorprofile.udp.client.UdpIntegrationClient;
import com.entappia.ei4ovisitorprofile.utils.Utils;
import com.entappia.ei4ovisitorprofile.dbmodels.Notifications;
import com.entappia.ei4ovisitorprofile.models.NotificationDetailMessages;
import com.entappia.ei4ovisitorprofile.repository.NotificationsRepository;

@Component
public class NotificationManager {
	
	private UdpClient mUdpClient = null;
	@Autowired
	private NotificationsRepository notificationRepository;
	
	ArrayList<Notification> notificationList = new ArrayList<>();
	
	int intervalBetweenNotifications = AppConstants.intervalbetweenNotifications;//Default in minutes
	int maxNotificationCount = AppConstants.maxNotificationCount;// Default
	int minutesToKeepPreviousNotification = AppConstants.minutesToKeepPreviousNotification;//Default
	
	/*@Autowired
	public NotificationManager() {
	}*/
	@Autowired
	public NotificationManager(UdpIntegrationClient udpClient) {
		this.mUdpClient = udpClient;
		Utils.printInConsole("Inside Notification Manager Constructor");
	}
	
	public void setIntervalBetweenNotifications(int intervalBetweenNotifications) {
		this.intervalBetweenNotifications = intervalBetweenNotifications;
	}


	public void setMaxNotificationCount(int maxNotificationCount) {
		this.maxNotificationCount = maxNotificationCount;
	}

	

	public void addNotification(String tagId, String assignedId, String sNotification, NotificationType errorType, String percentage, String zoneName, Date endTime, String macId)
	{
		boolean notificationFoundInList =  false;
		for(int i = 0; i < notificationList.size();i++)
		{
			Notification notification = notificationList.get(i);
		
			if(notification.getAssignedId().equals(assignedId) && notification.getTagId().equals(tagId) 
					&& notification.getNotification().equals(sNotification)) {
				notificationFoundInList = true;
				notification.setErrorType(errorType);
				notification.setLastOccuredTime(new Date());
				notification.setSendNotification(true);
				notification.setPercentage(percentage);
				notification.setZoneName(zoneName);
				notification.setEndTime(endTime);
				notification.setMacId(macId);
				//The last Notified Time and the Notification count is updated only during the 
				//time of sending notifications
				
			}
			
		}
		if(notificationFoundInList == false)
		{
			Notification notification = new Notification();
			notification.setAssignedId(assignedId);
			notification.setTagId(tagId);
			notification.setNotification(sNotification);
			notification.setErrorType(errorType);
			notification.setLastOccuredTime(new Date());
			notification.setSendNotification(true);
			notification.setPercentage(percentage);
			notification.setZoneName(zoneName);
			notification.setEndTime(endTime);
			notification.setMacId(macId);
			notificationList.add(notification);
		}
		
	}
	public void removeNotification(String tagId,String assignedId,String sNotification)
	{
		for(int i = notificationList.size() - 1; i >= 0;i--)
		{
			Notification notification = notificationList.get(i);
		
			if(notification.getAssignedId().equals(assignedId) && notification.getTagId().equals(tagId) 
					&& notification.getNotification().equals(sNotification)) {
				
				notificationList.remove(i);
			}
			
		}
	}
	public void sendNotification()
	{
		
		HashMap<String, List<HashMap<String, String>>> lLateVisitorNotificationTagsList = new HashMap<>();
		HashMap<String, List<HashMap<String, String>>> lRestrictedZoneVisitorNotificationTagsList = new HashMap<>();
		
		NotificationType notificationType = NotificationType.INFORMATION;
		
		for(int i = 0; i < notificationList.size();i++)
		{
			Notification notification = notificationList.get(i);
			
			if(notification.isSendNotification() == true) {
				
				//check here for time expire and count for sending notification
				
				if(canNotify(notification) ==  true ) {
					
					notification.setSendNotification(true);
					notification.setLastNotifiedTime(new Date());
					notification.setNotificationCount(notification.getNotificationCount() + 1);
					
					if(notification.getNotification().equalsIgnoreCase("latetag")){
						
						HashMap<String, String> notificationmap = new HashMap<>(); 
						notificationmap.put("tagStatus", "Late Visitor");
						notificationmap.put("tagName", notification.getTagId());
						notificationmap.put("macId", notification.getMacId());
						notificationmap.put("notificationCount", String.valueOf(notification.getNotificationCount()));
						Date endDate = notification.getEndTime();
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
						String sEndDate = formatter.format(endDate);
						notificationmap.put("time", sEndDate );
						
						if(lLateVisitorNotificationTagsList.containsKey(notification.getZoneName()))
						{
							List<HashMap<String, String>> mapList = lLateVisitorNotificationTagsList.get(notification.getZoneName());
							
							if(mapList==null)
								mapList = new ArrayList<>();
							
							mapList.add(notificationmap);
							lLateVisitorNotificationTagsList.put(notification.getZoneName(), mapList); 
							
						}else
						{
							List<HashMap<String, String>> mapList= new ArrayList<>();
							mapList.add(notificationmap);
							lLateVisitorNotificationTagsList.put(notification.getZoneName(), mapList); 
							
						}
						
						
					} 
					
					if(notification.getNotification().equalsIgnoreCase("restrictedZoneVisitor")){
						
						HashMap<String, String> notificationmap = new HashMap<>(); 
						notificationmap.put("tagStatus", "Restricted Zone Visitor");
						notificationmap.put("tagName", notification.getTagId());
						notificationmap.put("macId", notification.getMacId());
						notificationmap.put("notificationCount", String.valueOf(notification.getNotificationCount()));
						Date endDate = notification.getEndTime();
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
						String sEndDate = formatter.format(endDate);
						notificationmap.put("time", sEndDate );
						
						if(lRestrictedZoneVisitorNotificationTagsList.containsKey(notification.getZoneName()))
						{
							List<HashMap<String, String>> mapList = lRestrictedZoneVisitorNotificationTagsList.get(notification.getZoneName());
							
							if(mapList==null)
								mapList = new ArrayList<>();
							
							mapList.add(notificationmap);
							lRestrictedZoneVisitorNotificationTagsList.put(notification.getZoneName(), mapList); 
							
						}else
						{
							List<HashMap<String, String>> mapList= new ArrayList<>();
							mapList.add(notificationmap);
							lRestrictedZoneVisitorNotificationTagsList.put(notification.getZoneName(), mapList); 
							
						}
						
						
					}
					
					if( notification.getErrorType().compareTo(notificationType) > 0) {
						notificationType = notification.getErrorType();
					}
				}
				
								
				//The last Notified Time and the Notification count is updated only during the 
				//time of sending notifications
				
			}
			else
			{
				notification.setSendNotification(false);
			}
			
		}
		
		if (lLateVisitorNotificationTagsList.size() > 0) {
			JSONObject mJSONArray = new JSONObject(lLateVisitorNotificationTagsList);

			JSONObject jsonObject = new JSONObject();
			//jsonObject.put("status", notificationType.name());
			jsonObject.put("status", 200);
			jsonObject.put("type", "lateTag");
			jsonObject.put("data", mJSONArray);
			mUdpClient.sendMessage(jsonObject.toString());
			
			NotificationDetailMessages notificationDetailMessages = new NotificationDetailMessages();
			notificationDetailMessages.setType("lateTag");
			notificationDetailMessages.setTagData(lLateVisitorNotificationTagsList);
			
			Notifications notifications = new Notifications();
			notifications.setType(notificationType);
			notifications.setSource("visitor_profile");
			
			Integer sum = lLateVisitorNotificationTagsList.values().stream().mapToInt(List::size).sum();
			
			//if(lLateVisitorNotificationTagsList.size()>1)
			if(sum > 1)
			{
				notifications.setMessage(sum + " visitors allocated time period exceeded");
				notifications.setMessageObj(notificationDetailMessages);
			}
			else{  
				 
				 
				 Map.Entry<String, List<HashMap<String, String>>> entry = lLateVisitorNotificationTagsList.entrySet().iterator().next();
				 String zoneName = entry.getKey();
				 List<HashMap<String, String>> value = entry.getValue();
				 if(value.size()>1) {
					  notifications.setMessage(value.size() + " visitors allocated time period exceeded");
					  notifications.setMessageObj(notificationDetailMessages);
				 }else
				 {
					 notifications.setMessage("visitor " + value.get(0).get("tagName") + " allocated time period exceeded " +
							zoneName);
					notifications.setMessageObj(null);
				 }
			}

			notifications.setNotifyTime(new Date());
			notificationRepository.save(notifications);
		}
		if (lRestrictedZoneVisitorNotificationTagsList.size() > 0) {
			JSONObject mJSONArray = new JSONObject(lRestrictedZoneVisitorNotificationTagsList);

			JSONObject jsonObject = new JSONObject();
			//jsonObject.put("status", notificationType.name());
			jsonObject.put("status", 200);
			jsonObject.put("type", "restrictedZoneVisitor");
			jsonObject.put("data", mJSONArray);
			mUdpClient.sendMessage(jsonObject.toString());
			
			NotificationDetailMessages notificationDetailMessages = new NotificationDetailMessages();
			notificationDetailMessages.setType("restrictedZoneVisitor");
			notificationDetailMessages.setTagData(lRestrictedZoneVisitorNotificationTagsList);
			
			Notifications notifications = new Notifications();
			notifications.setType(notificationType);
			notifications.setSource("visitor_profile");
			
			Integer sum = lRestrictedZoneVisitorNotificationTagsList.values().stream().mapToInt(List::size).sum();
			
			//if(lRestrictedZoneVisitorNotificationTagsList.size()>1)
			if(sum > 1)
			{
				notifications.setMessage(sum + " tags found");
				notifications.setMessageObj(notificationDetailMessages);
			}
			else{  
				 
				 
				 Map.Entry<String, List<HashMap<String, String>>> entry = lRestrictedZoneVisitorNotificationTagsList.entrySet().iterator().next();
				 String zoneName = entry.getKey();
				 List<HashMap<String, String>> value = entry.getValue();
				 if(value.size()>1) {
					  notifications.setMessage(value.size() + " visitors in restricted zone");
					  notifications.setMessageObj(notificationDetailMessages);
				 }else
				 {
					notifications.setMessage("Visitor " + value.get(0).get("tagName") + " in restricted zone " +
							zoneName);
					notifications.setMessageObj(null);
				 }
			}

			notifications.setNotifyTime(new Date());
			notificationRepository.save(notifications);
		}
		removeExpiredNotifications();	
	}
	private void removeExpiredNotifications() {
		for(int i = notificationList.size() - 1; i >= 0;i--)
		{
			Notification notification = notificationList.get(i);
			Date lastNotifiedTime = notification.getLastNotifiedTime();
			Date currentTime = new Date();
			long duration = currentTime.getTime() - lastNotifiedTime.getTime();
			
			long diffMinutes = duration / (60 * 1000) % 60; 
			
			if(diffMinutes > minutesToKeepPreviousNotification) {
				
				notificationList.remove(i);
			}
			
		}
	}

	private boolean canNotify(Notification notification) {
	
		//Check all the possible conditions to send notification and returns
		//whether particular notification can be sent or not
		if(notification.getLastNotifiedTime() != null)
		{
		Date lastNotified = notification.getLastNotifiedTime();
		Date currentDate = new Date();
		long diff = currentDate.getTime() - lastNotified.getTime();
		
		//long diffSeconds = diff / 1000 % 60;  
		long diffMinutes = diff / (60 * 1000) % 60; 
		//long diffHours = diff / (60 * 60 * 1000);
		if(notification.getNotificationCount() < maxNotificationCount &&  diffMinutes >= intervalBetweenNotifications)
		{
			return true;
		}
		}
		else 
		{
			return true;
		}
		return false;
	}

	public void setUdpClient(UdpIntegrationClient udpClient) {
		
		this.mUdpClient = udpClient;
	}
	
	

}
