package com.entappia.ei4ovisitorprofile.datacollection;

import java.sql.Time;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4ovisitorprofile.repository.TableUtils;
import com.entappia.ei4ovisitorprofile.dbmodels.ProfileConfiguration;
import com.entappia.ei4ovisitorprofile.models.ConfigData;
import com.entappia.ei4ovisitorprofile.repository.ProfileConfigurationRepository;
import com.entappia.ei4ovisitorprofile.constants.AppConstants;
import com.entappia.ei4ovisitorprofile.constants.AppConstants.LogEventType;
import com.entappia.ei4ovisitorprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4ovisitorprofile.constants.AppConstants.ZoneType;
import com.entappia.ei4ovisitorprofile.dbmodels.CampusDetails;
import com.entappia.ei4ovisitorprofile.dbmodels.Tags;
import com.entappia.ei4ovisitorprofile.dbmodels.Visitor;
import com.entappia.ei4ovisitorprofile.dbmodels.VisitorLogs;
import com.entappia.ei4ovisitorprofile.dbmodels.ZoneDetails;
import com.entappia.ei4ovisitorprofile.dbmodels.Shift;
import com.entappia.ei4ovisitorprofile.quuppa.QuuppaApiService;
import com.entappia.ei4ovisitorprofile.repository.CampusDetailsRepository;
import com.entappia.ei4ovisitorprofile.repository.TagsRepository;
import com.entappia.ei4ovisitorprofile.repository.VisitorLogsRepository;
import com.entappia.ei4ovisitorprofile.repository.ZoneRepository;
import com.entappia.ei4ovisitorprofile.repository.ShiftRepository;
import com.entappia.ei4ovisitorprofile.repository.VisitorRepository;
import com.entappia.ei4ovisitorprofile.udp.client.UdpClient;
import com.entappia.ei4ovisitorprofile.udp.client.UdpIntegrationClient;
import com.entappia.ei4ovisitorprofile.utils.LogEvents;
import com.entappia.ei4ovisitorprofile.utils.Preference;
import com.entappia.ei4ovisitorprofile.utils.Utils;
import com.google.gson.Gson;
import com.snatik.polygon.Point;
import com.snatik.polygon.Polygon;

@Component
public class TagScheduleAsyncTask {

	public String TAG_NAME = "TagScheduleTask";

	Preference preference = new Preference();

	@Autowired
	TableUtils tableUtils;
	
	@Autowired
	private QuuppaApiService quuppaApiService;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private CampusDetailsRepository campusDetailsRepository;

	@Autowired
	private ZoneRepository zoneRepository;
	
	
	@Autowired
	private VisitorLogsRepository visitorLogsRepository;
	

	@Autowired
	private ProfileConfigurationRepository profileConfigurationRepository;
	
	@Autowired
	private LogEvents logEvents;
	
	@Autowired
	private ShiftRepository shiftRepository;
	
	@Autowired
	private VisitorRepository visitorRepository;

	private final UdpClient udpClient;

	@Autowired
	private NotificationManager notificationManager ;
	
	//move these functions to a separate class
	//
	private HashMap<String, List<String>> gridZoneMap;
	private JSONArray tagsJsonArray = null;
	private CampusDetails campusDetails = null;
	private JSONArray smoothedPosition = null;
	
	int maxDaysToKeepOldTables = AppConstants.maxDaysToKeepOldTables;
	int threadTimeDuration = AppConstants.threadTimeDuration;
	int maxDaysToKeepOldLogData = AppConstants.maxDaysToKeepOldLogData;
	
	LocalDate tablesDeletedDate = null;
	boolean deleteOldTables = true;
	LocalDate logsDeletedDate = null;
	boolean deleteOldLogs = true;
	
	private ScheduledExecutorService scheduler;
	private ScheduledFuture<?> futureTask = null;
	private Runnable myTask;
	
	@Autowired
	public TagScheduleAsyncTask(UdpIntegrationClient udpClient) {
		this.udpClient = udpClient;
	}

	@PostConstruct
	private void intializeThread()
	{
		scheduler =
				 Executors.newScheduledThreadPool(2);
		
		myTask = new Runnable()
		{
			@Override
			public void run()
			{
				getTagPositions();
			}
		};
		if(threadTimeDuration > 0)
	    {       
	        if (futureTask != null)
	        {
	            futureTask.cancel(true);
	        }

	        futureTask = scheduler.scheduleAtFixedRate(myTask, 0, threadTimeDuration * 60, TimeUnit.SECONDS);
	    }
		
	}
	int getReadInterval()
	{
		//read here from repository for changes in thread timing and return
		//need to check if 0 the thread has to be stopped or not
		ProfileConfiguration profileConfiguration = profileConfigurationRepository.selectTopRecord();
		int threadTimeDurationLocal = -1;
		if(null != profileConfiguration) {

			List<ConfigData> configDataList = profileConfiguration.getConfigData();
			if (configDataList != null && configDataList.size() > 0) {
				for (int i = 0; i < configDataList.size(); i++) {

					ConfigData configData = configDataList.get(i);

					if (configData != null) {
						 if (configData.getName().equals(AppConstants.CONFIG_VIS)) {
							threadTimeDurationLocal = Integer.parseInt(configData.getDefaultval());
							if(threadTimeDurationLocal <= 0)
							{
								threadTimeDurationLocal = -1;
							}
							Utils.printInConsole("ThreadTimeDuration : "+ threadTimeDurationLocal);
						}
					}
				}
			}
		}

	
		return threadTimeDurationLocal;
	}
	public void checkAndChangeReadInterval()
	{
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {
			public void run() {
				int currentReadInterval = getReadInterval();
				Utils.printInConsole("current thread Interval" + currentReadInterval);
				if(-1 != currentReadInterval) {
					if(currentReadInterval != threadTimeDuration) {
						threadTimeDuration = currentReadInterval;
						changeReadInterval(threadTimeDuration * 60);
					}
				}
			}
		});
		executorService.shutdown();
	}
	public void changeReadInterval(long time)
	{
	    if(time > 0)
	    {       
	        if (futureTask != null)
	        {
	            futureTask.cancel(true);
	        }

	        futureTask = scheduler.scheduleAtFixedRate(myTask, time, time, TimeUnit.SECONDS);
	    }
	}
	
	Gson gson = new Gson();
	DecimalFormat df = new DecimalFormat("#.##");
	
	private boolean getTagListFromDevice()
	{
		try {
			tagsJsonArray = null;
		CompletableFuture<JSONObject> getTagCompletableFuture = quuppaApiService.getTagDetails();
		CompletableFuture.allOf(getTagCompletableFuture).join();

		if (getTagCompletableFuture.isDone()) {
			preference.setLongValue("lastscan", System.currentTimeMillis());

			JSONObject jsonObject = getTagCompletableFuture.get();
			if (jsonObject != null) {

				String status = jsonObject.optString("status");

				if (!Utils.isEmptyString(status)) {
					if (status.equals("success")) {

						JSONObject jsonTagObject = jsonObject.optJSONObject("response");
						if (jsonTagObject != null) {
							int code = jsonTagObject.getInt("code");
							if (code == 0) {
								tagsJsonArray = jsonTagObject.getJSONArray("tags");
							}
						}
					}
					else if (status.equals("error")) {
						String message = jsonObject.getString("message");
						logEvents.addLogs(LogEventType.ERROR, "GetQuuppaTag", "Get QuuppaTag-Schedule", message);

					}
				}
			}
		}
		}
		catch(Exception e) {
			logEvents.addLogs(LogEventType.ERROR, "GetQuuppaTag", "Get QuuppaTag-Schedule", e.getMessage());
			e.printStackTrace();
		}
		if(tagsJsonArray!= null)
			return true;
		return false;
	}
	private boolean getCampusDetails() {
		campusDetails = null;
		campusDetails = getCampusDetails(1);
		if(campusDetails != null) {
			return true;
		}
		return false;
	}
	private boolean searchTag(String macId) {
		smoothedPosition = null;
		for(int i=0; i<tagsJsonArray.length(); i++){
			JSONObject tagJsonObject = tagsJsonArray.getJSONObject(i);
			String id = tagJsonObject.optString("tagId", "");
			
			if(macId.equals(id))
			{
				smoothedPosition = tagJsonObject
						.optJSONArray("location");
				if (smoothedPosition != null)
					return true;
				else
					return false;
			}
		}
	
		return false;
	}
	private double getSmoothedPositionX() {
		if (smoothedPosition != null)
			return smoothedPosition.getDouble(0);
		return -1;
	}
	private double getSmoothedPositionY() {
		if (smoothedPosition != null)
			return smoothedPosition.getDouble(1);
		return -1;
	}
	private boolean getGridZoneMap() {
		
		gridZoneMap = new HashMap<>();
		if (campusDetails != null)
			gridZoneMap = campusDetails.getGridZoneDetails();
		else
			return false;
		if(gridZoneMap!= null) {
			return true;
		}
		return false;
	}
	private double getOriginX() {
		if(campusDetails!= null)
			return campusDetails.getOriginX()
				* campusDetails.getMetersPerPixelX();
		return -1;
	}
	private double getOriginY() {
		if(campusDetails!= null)
			return campusDetails.getOriginY()
				* campusDetails.getMetersPerPixelY();
		return -1;
	}
	//@Scheduled(cron = "0/10 * * * * *")
	//@Scheduled(cron = "0 0/1 * * * *")
	public void getTagPositions() {

		Calendar currentCalendar = Calendar.getInstance();
		currentCalendar.set(Calendar.MILLISECOND, 0);
		currentCalendar.set(Calendar.SECOND, 0);
 		Date currentDate = currentCalendar.getTime();
 		
		Utils.printInConsole("Visitor Run Time:-" + Utils.getFormatedDate2(new Date()), true);

							
				ArrayList<Tags> tagList = new ArrayList<Tags>();
				double originX = getOriginX();
				double originY = getOriginY();

				try {
					
					//Get current shift name
					//If there are no active shifts the shift name 
					//will be empty. quit the process or loop when no shift is 
					//found make this function call to the start of the 
					//thread and get out complete thread execution
					Utils.printInConsole("GetCurrentShiftname");
					String currentShiftName = getCurrentShiftName(currentDate);
					if(currentShiftName == "") {
						Utils.printInConsole("No shift available for current time");
						logEvents.addLogs(LogEventType.EVENT, "Run", "Run-VisitorprofileApplication",
								"No shift available for current time");
						currentShiftName = "NoShift";
						//comment this return when data need to be collected during no shift time also
						return;
					}
						
					Utils.printInConsole(currentShiftName);
					//append date and shift name as per table strategy
					Utils.printInConsole("GetCurrentTablename");
					String currentTableName = GetCurrentTableName(currentShiftName);
					if(currentTableName == "") {
						Utils.printInConsole("Table Name Formatting error");
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-VisitorprofileApplication",
								"Table Name formatting error");
						return;
					}
					Utils.printInConsole(currentTableName);
					//Check whether table exists
					//Create new table if not exists
					Utils.printInConsole("CreateVisitorLocationtable");
					tableUtils.createLocationTable(currentTableName);
					
					//fetch all tags allocated or mapped to Visitor from server/DB
					//fetch from QPE one time for all the iterations
					//fetch from qpe
					Utils.printInConsole("GetTaglistfromdevice");
					if(!getTagListFromDevice()) {
						Utils.printInConsole("taglist from device not found");
						return;
					}
					
					Utils.printInConsole("GetCampusdetails");
					if(!getCampusDetails()) {
						Utils.printInConsole("Campusdetails Not Found");
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-VisitorprofileApplication",
								"Campus Details not found");
						return;
					}
					Utils.printInConsole("GetGridzonemap");
					if(!getGridZoneMap()){
						Utils.printInConsole("Gridzone map Not Found");
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-VisitorprofileApplication",
								"Grid zone map not found");
						return;
					}
					Utils.printInConsole("GetOriginX");
					originX = getOriginX();
					if(originX == -1) {
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-VisitorprofileApplication",
								"originX not Found ");
						return;
					}
					Utils.printInConsole("GetOriginY");
					originY = getOriginY();
					if(originY == -1) {
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-VisitorprofileApplication",
								"originX not Found");
						return;
					}
					//HashMap<String, Tags> tagsMap = tagEvents.getDBTags();
					//Get all other values required for notification etc
					//loop for all visitor tags
					
					//tagList = (ArrayList<Tags>) tagsRepository.selectVisitorTags("Assigned");
					tagList = (ArrayList<Tags>) tagsRepository.selectVisitorTags();
					 
					for (int i = 0;i < tagList.size();i++){
						
						Tags visitorTag = tagList.get(i);
						
						Visitor assignedVisitor = visitorRepository.getVisitorByVisitorId(visitorTag.getAssignmentId());
						
						
						//If the tag type is visitor and its status is not assigned then send error
						
						String tagStatus = visitorTag.getStatus();
						if(tagStatus.equalsIgnoreCase("Assigned") != true){
							//if tag type is set to a visitor and tag status is not assigned then show error
							//notify
							//notificationManager.addNotification(visitorTag.getTagId(), visitorTag.getAssignmentId(), "Visitor status mismatch", NotificationType.WARNING);
							
							Utils.printInConsole("Visitor type is set in tag but status is not assigned");
							//continue;
							
						}
						else {
						//notify
						//notificationManager.removeNotification(visitorTag.getTagId(), visitorTag.getAssignmentId(), "Visitor status mismatch");
						}
						
						/*if (sType.equalsIgnoreCase("visitor") || sType.equalsIgnoreCase("Contractor")
								|| sType.equalsIgnoreCase("Vendor"))
						
						"Lost", "Not Working",
						"Blocked", "Expired"	*/		
						
						double x = -1;
						double y = -1;
						
						
						if(searchTag(visitorTag.getMacId())) {
							
							x = getSmoothedPositionX();
							y = getSmoothedPositionY();
							
							x = Math.abs(originX - x);
							y = Math.abs(originY - y);
							
							//notify
							//notificationManager.removeNotification(visitorTag.getTagId(), visitorTag.getAssignmentId(), "Tag location not found");
						}
						else { 
							//tag not found
							logEvents.addLogs(LogEventType.ERROR, "Run", "Run-VisitorprofileApplication",
									"Tag : " + visitorTag.getTagId() + " assigned to an Employee :" + visitorTag.getAssignmentId() + " is missing" );
							Utils.printInConsole("tag Not Found");
							//notify
							//notificationManager.addNotification(visitorTag.getTagId(), visitorTag.getAssignmentId(), "Tag location not found", NotificationType.WARNING);
							//return;
						}
						Utils.printInConsole("GetZoneDetails");
						//check tag exists
						ZoneDetails zoneDetails = getZoneDetails(gridZoneMap, x, y);
						Utils.printInConsole("GetZoneDetails x: " + x + " y: " + y);
						if (zoneDetails != null){
						ZoneType currentZoneType = zoneDetails.getZoneType();
						String zoneName = zoneDetails.getName();
						
						
						Date currentTime = new Date();
						Date visitorFromTime = assignedVisitor.getFromTime();
						Date visitorToTime = assignedVisitor.getToTime();
						Date visitorDate = assignedVisitor.getDate();
						//if a visitor is not in his allowable time zone
						// then send notification
						Utils.printInConsole("CurrentTime : " + currentTime + " VisitorFromTime" + visitorFromTime + " visitorToTime" + visitorToTime);
						if (checkAllowedTime(currentTime, visitorFromTime, visitorToTime, visitorDate) == false){
							//notify
							Calendar calendar = Calendar.getInstance();
							calendar.clear();
					        calendar.setTime(visitorToTime);
					    	calendar.set(visitorDate.getYear() + 1900,visitorDate.getMonth() , visitorDate.getDate());
					    	visitorToTime = calendar.getTime();
							notificationManager.addNotification(visitorTag.getTagId(), visitorTag.getAssignmentId(), "latetag", NotificationType.EMERGENCY, 
									"", zoneName, visitorToTime , visitorTag.getMacId());
							
							Utils.printInConsole("Visitor time limit crossed");
							//continue;
						}
						else{
							//notify
							notificationManager.removeNotification(visitorTag.getTagId(), visitorTag.getAssignmentId(), "latetag");
						}
						
						if(currentZoneType == ZoneType.DeadZone){
							Utils.printInConsole("Tag assigned is in Dead Zone");
							//notificationManager.addNotification(visitorTag.getTagId(), visitorTag.getAssignmentId(), "Tag assigned is in Dead Zone", NotificationType.INFORMATION);
							//continue;
						}
						else{
							//notificationManager.removeNotification(visitorTag.getTagId(), visitorTag.getAssignmentId(), "Tag assigned is in Dead Zone");
						}
						List<String> assignedAreas = assignedVisitor.getAccessLimit();
						if(assignedAreas.size() == 0)
						{
							//show error and continue
							Utils.printInConsole("No zones is assigned to visitor but he not in dead zone");
							//continue;
						}
						else
						{
						boolean visitorInAllowedzone = false;
						for(int j = 0; j < assignedAreas.size();j++)
						{
							if(zoneName.equals(assignedAreas.get(j)))
							{
								//show error and continue
								visitorInAllowedzone = true;
								break;
							}
						}
						if(visitorInAllowedzone == false)
						{
							Utils.printInConsole("Visitor is in Restricted Zone");
							//notify
							notificationManager.addNotification(visitorTag.getTagId(), visitorTag.getAssignmentId(), "restrictedZoneVisitor", NotificationType.EMERGENCY, "", zoneName, currentTime, visitorTag.getMacId() );
						}
						else
						{
							//notify
							notificationManager.removeNotification(visitorTag.getTagId(), visitorTag.getAssignmentId(), "restrictedZoneVisitor");
						}
						}										
						if (zoneDetails != null && currentZoneType != ZoneType.DeadZone) {
							/*Utils.printInConsole("Time in Minutes : " + Utils.getDayMintus(currentDate));
							Utils.printInConsole("Visitor ID : " + visitorTag.getAssignmentId());
							Utils.printInConsole("Zone ID : " + zoneDetails.getId());
							Utils.printInConsole("Zone Name : " + zoneName);
							Utils.printInConsole("Grid xPosition : " + getGridPosition(x));
							Utils.printInConsole("Grid yposition : " + getGridPosition(y));
							Utils.printInConsole("Grid Number : " + ((getGridPosition(x)-1) * Math.ceil(campusDetails.getWidth()) + getGridPosition(y)));*/
							
							int GridNumber = (int) ((getGridPosition(x)-1) * Math.ceil(campusDetails.getWidth()) + getGridPosition(y));
							
							int xValue = getGridPosition(x);
							int yValue = getGridPosition(y);

							String gridPosition = xValue + "_" + yValue;
							
							String VisitorTagAssignmentID = visitorTag.getAssignmentId();
							int zoneId = zoneDetails.getId();
							int currentTimeinMinutes = Utils.getDayMintus(currentDate);
							
							tableUtils.insertIntoLocationTable(currentTableName, VisitorTagAssignmentID, currentTimeinMinutes,
									GridNumber, zoneId,zoneName, gridPosition, threadTimeDuration);
														
						} else {
							//Tag in Dead zone 
							Utils.printInConsole("Visitor is in Non Tracking Zone (Dead zone)");
							//return;
							logEvents.addLogs(LogEventType.EVENT, "Run", "Run-VisitorprofileApplication",
										"Visitor is in Non Tracking Zone (Dead zone)" );
						}
						
						//write all the details required to the table
						//send required notification to server through UDP	
						
					}
					
					else
					{
						Utils.printInConsole("zone details not found");
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-VisitorprofileApplication",
								"ZoneDetails x: " + x + " y: " + y + "is not found" );
					
					}
				}
			}
				catch(Exception e) {
						
				}
														
				notificationManager.sendNotification();
				checkAndChangeReadInterval();
				checkAndModifyConfigurationData();
				modifyTableData();
			
}

	protected boolean checkAllowedTime(Date currentTime, Date visitorFromTime, Date visitorToTime, Date visitorDate) {


		Utils.printInConsole("Inside checkAllowed Time");
		Utils.printInConsole("CurrentTime : " + currentTime + " VisitorFromTime" + visitorFromTime + " visitorToTime" + visitorToTime);
		
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
        calendar.setTime(visitorFromTime);
        calendar.set(visitorDate.getYear() + 1900,visitorDate.getMonth() , visitorDate.getDate());
        visitorFromTime = calendar.getTime();
        
        calendar.clear();
        calendar.setTime(visitorToTime);
    	calendar.set(visitorDate.getYear() + 1900,visitorDate.getMonth() , visitorDate.getDate());
    	visitorToTime = calendar.getTime();
        
    	Utils.printInConsole("After converting");
    	Utils.printInConsole("CurrentTime : " + currentTime + " VisitorFromTime" + visitorFromTime + " visitorToTime" + visitorToTime);
    	
        if(currentTime.after(visitorFromTime) && currentTime.before(visitorToTime)){
			  return true;
        }
		return false;
	}
	protected boolean checkAllowedTimeNew(Date currentTime, Date visitorFromTime, Date visitorToTime, Date visitorDate) {


		Utils.printInConsole("Inside checkAllowedTime");
		Utils.printInConsole("CurrentTime : " + currentTime + " visitorFromTime" + visitorFromTime + " visitorToTime" + visitorToTime);

		Date visitorFromTime1,visitorToTime1;
		
		Calendar calendar = Calendar.getInstance();
		TimeZone tz = Calendar.getInstance().getTimeZone();
		
		calendar.clear();
		calendar.setTimeZone(tz) ;
		if (tz.useDaylightTime()) {
			visitorFromTime.setHours(visitorFromTime.getHours()-(tz.getDSTSavings()/3600000));
			visitorToTime.setHours(visitorToTime.getHours()-(tz.getDSTSavings()/3600000));
		}
		
		
		calendar.setTime(visitorFromTime);
		calendar.set(visitorDate.getYear() + 1900,visitorDate.getMonth() , visitorDate.getDate());
		visitorFromTime1 = calendar.getTime();

		calendar.clear();
		calendar.setTimeZone(tz) ;
		calendar.setTime(visitorToTime);
		calendar.set(visitorDate.getYear() + 1900,visitorDate.getMonth() , visitorDate.getDate());
		visitorToTime1 = calendar.getTime();

		Utils.printInConsole("After converting");
		Utils.printInConsole("CurrentTime : " + currentTime + " assetFromTime" + visitorFromTime + " assetToTime" + visitorToTime);
		
		if(currentTime.after(visitorFromTime1) && currentTime.before(visitorToTime1)){
			return true;
		}
		return false;
	
		
		/*Utils.printInConsole("Inside checkAllowed Time");
		Utils.printInConsole("CurrentTime : " + currentTime + " VisitorFromTime" + visitorFromTime + " visitorToTime" + visitorToTime);
		
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
        calendar.setTime(visitorFromTime);
        calendar.set(visitorDate.getYear() + 1900,visitorDate.getMonth() , visitorDate.getDate());
        visitorFromTime = calendar.getTime();
        
        calendar.clear();
        calendar.setTime(visitorToTime);
    	calendar.set(visitorDate.getYear() + 1900,visitorDate.getMonth() , visitorDate.getDate());
    	visitorToTime = calendar.getTime();
        
    	Utils.printInConsole("After converting");
    	Utils.printInConsole("CurrentTime : " + currentTime + " VisitorFromTime" + visitorFromTime + " visitorToTime" + visitorToTime);
    	
        if(currentTime.after(visitorFromTime) && currentTime.before(visitorToTime)){
			  return true;
        }
		return false;*/
	}
	//GetCurrentTableName
	//converts string according to the table name rules
	//need to handle this issue if no shift is found 
	private String GetCurrentTableName(String shiftName){
		String currentTableName = "";
		String profileName = "Visitor_Location";
		Date currentDate = new Date();
		
		//Table name hours and minutes can be used to find out in case of issues
		//For only date is used we can change it later
		//SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy HH:mm:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
	    String scurrentDate = formatter.format(currentDate);
	    
	    currentTableName = profileName + "_" + scurrentDate + "_" + shiftName;

		return currentTableName;
		
	}
	
	//GetCurrentShiftName
		//The current working shift is needed to append in the table name 
		//to find the current working shift the current time is checked 
		//whether it lies between shift start time shift end time 
		// the issue occurs when two shifts are running in the same time
		// have to handle it 
		private String getCurrentShiftName(Date currentTime)
		{
			//Get current shift name from the shift start time and 
			ArrayList<Shift> shiftDetailsArr = (ArrayList<Shift>) shiftRepository.findActiveShifts();
			
				
			    String shiftName = "";
				for (int i = 0;i < shiftDetailsArr.size();i++){
					Shift shiftElement = shiftDetailsArr.get(i);
					Date shiftStartTime = shiftElement.getShiftStartTime();
					Date shiftEndTime = shiftElement.getShiftEndTime();
					
					Utils.printInConsole("shiftStartTime" + shiftStartTime);
					Utils.printInConsole("shiftEndTime" + shiftEndTime);
					
					Calendar calendar = Calendar.getInstance();
					calendar.clear();
			        calendar.setTime(shiftStartTime);
			        calendar.set(currentTime.getYear() + 1900,currentTime.getMonth() , currentTime.getDate());
			        shiftStartTime = calendar.getTime();
			        
			        calendar.clear();
			        calendar.setTime(shiftEndTime);
			    	calendar.set(currentTime.getYear() + 1900,currentTime.getMonth() , currentTime.getDate());
			        shiftEndTime = calendar.getTime();
			        
			        if(currentTime.after(shiftStartTime) && currentTime.before(shiftEndTime)){
						  shiftName = shiftElement.getShiftName(); break; }
			        if(currentTime.equals(shiftStartTime)){
			    		shiftName = shiftElement.getShiftName();
			    		break;
			    	}
						
				}
			
				return shiftName;
			}

	private ZoneDetails getZoneDetails(HashMap<String, List<String>> gridZoneMap, double x, double y) {

		int xValue = getGridPosition(x);
		int yValue = getGridPosition(y);

		if (gridZoneMap.containsKey(xValue + "_" + yValue)) {

			List<String> zoneIdList = gridZoneMap.get(xValue + "_" + yValue);
			if (zoneIdList != null && zoneIdList.size() > 0) {
				if (zoneIdList.size() == 1) {
					String zoneId = zoneIdList.get(0);
					Optional<ZoneDetails> zoneDetails = zoneRepository.findById(Integer.valueOf(zoneId));
					if (zoneDetails != null && zoneDetails.isPresent() && zoneDetails.get().isActive())
						return zoneDetails.get();
				} else {
					for (String zoneId : zoneIdList) {
						Optional<ZoneDetails> zoneDetails = zoneRepository.findById(Integer.valueOf(zoneId));
						if (zoneDetails != null && zoneDetails.isPresent() && zoneDetails.get().isActive()) {

							ZoneDetails zoneDetails1 = zoneDetails.get();
							List<HashMap<String, String>> coordinateList = zoneDetails1.getCoordinateList();
							if (isInsidePolygon(coordinateList, x, y))
								return zoneDetails1;
						}
					}
				}
			}
		}

		return null;
	}

	private boolean isInsidePolygon(List<HashMap<String, String>> coordinateList, double x, double y) {
		Polygon.Builder polygonBuilder = Polygon.Builder();

		for (int j = 0; j < coordinateList.size(); j++) {
			HashMap<String, String> coordinate = coordinateList.get(j);

			double x1 = Double.valueOf(coordinate.get("x"));
			double y1 = Double.valueOf(coordinate.get("y"));

			polygonBuilder.addVertex(new Point(x1, y1));

		}

		polygonBuilder.close();

		Polygon polygon = polygonBuilder.build();
		Point point = new Point(x, y);
		boolean contains = polygon.contains(point);
		if (contains) {
			return true;
		} else
			return false;
	}

	private int getGridPosition(double num1) {
		int value = 0;
		if (num1 % 1 == 0)
			value = (int) Math.floor(num1);
		else
			value = (int) Math.floor(num1 + 1);

		return value - 1;
	}
 
 

	public CampusDetails getCampusDetails(int campusId) {
		return campusDetailsRepository.findByCampusId(campusId);
	}

	public HashMap<String, List<String>> getCampusGridList() {
		//gridZoneMap = new HashMap<>();
		//campusDetails = campusDetailsRepository.findByCampusId(1);
		if (campusDetails != null)
			gridZoneMap = campusDetails.getGridZoneDetails();

		return gridZoneMap;
	}
	void checkAndModifyConfigurationData() {
		try {
			ProfileConfiguration profileConfiguration = profileConfigurationRepository.selectTopRecord();
			if(null != profileConfiguration) {
			List<ConfigData> configDataList = profileConfiguration.getConfigData();
			if (configDataList != null && configDataList.size() > 0) {
				for (int i = 0; i < configDataList.size(); i++) {

					ConfigData configData = configDataList.get(i);

					if (configData != null) {

						if (configData.getName().equals(AppConstants.CONFIG_TTA))
						{
							maxDaysToKeepOldTables = Integer.parseInt(configData.getDefaultval());
							if(maxDaysToKeepOldTables >= AppConstants.minDaysToKeepOldTables && maxDaysToKeepOldTables <= AppConstants.maxDaysToKeepOldTables)
							{
								if(maxDaysToKeepOldTables != AppConstants.maxDaysToKeepOldTables) {
									deleteOldTables = true;
								}
								
								if(null == tablesDeletedDate)
								{
									deleteOldTables = true;
								}
								else {//database deleted date not NULL
									LocalDate currentDate = LocalDate.now();
									if(!tablesDeletedDate.isEqual(currentDate)) {
										deleteOldTables = true;
									}
								}

							}
							else
							{
								maxDaysToKeepOldTables = AppConstants.maxDaysToKeepOldTables;
								deleteOldTables = true;
							}

						}
						else if (configData.getName().equals(AppConstants.CONFIG_LTA))
						{
							maxDaysToKeepOldLogData = Integer.parseInt(configData.getDefaultval());

							if(maxDaysToKeepOldTables >= AppConstants.minDaysToKeepOldTables && maxDaysToKeepOldTables <= AppConstants.maxDaysToKeepOldTables)
							{
								if(maxDaysToKeepOldLogData != AppConstants.maxDaysToKeepOldLogData) {
									deleteOldLogs = true;
								}
								if(null == logsDeletedDate)
								{
									deleteOldLogs = true;
								}
								else {//database deleted date not NULL
									LocalDate currentDate = LocalDate.now();
									if(!logsDeletedDate.isEqual(currentDate)) {
										deleteOldLogs = true;
									}
								}
							}
							else
							{
								maxDaysToKeepOldLogData = AppConstants.maxDaysToKeepOldLogData;
								deleteOldLogs = true;
							}

						}
						else if (configData.getName().equals(AppConstants.CONFIG_ARI)) 
						{
							int intervalBetweenNotifications = Integer.parseInt(configData.getDefaultval());
							if(intervalBetweenNotifications > 0)
							{
								notificationManager.setIntervalBetweenNotifications(intervalBetweenNotifications);
							}
							Utils.printInConsole("intervalBetweenNotifications : "+ intervalBetweenNotifications);
						}
						else if (configData.getName().equals(AppConstants.CONFIG_ARC)) {
							int maxNotificationCount = Integer.parseInt(configData.getDefaultval());
							if(maxNotificationCount >= 0)
							{
								notificationManager.setMaxNotificationCount(maxNotificationCount);
							}
							Utils.printInConsole("maxNotificationCount : "+ maxNotificationCount);
						}

					}

				}
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	void modifyTableData(){

		if(deleteOldTables) {
			LocalDate lastDateToKeepLocationTable = getLastDateToKeepLocationTable(maxDaysToKeepOldTables);
			deleteOldLocationTable(lastDateToKeepLocationTable);
			//ToDo check for if there are error conditons and set flag to false
			Utils.printInConsole("maxDaysToKeepOldTables : " + maxDaysToKeepOldTables + "Date : "+ lastDateToKeepLocationTable + "deleteOldTables : "+ deleteOldTables);
			deleteOldTables = false;
		}
		if(deleteOldLogs) {
			LocalDate lastDateToKeepLogData = getLastDateToKeepLogData(maxDaysToKeepOldLogData);
			deleteOldLogs(lastDateToKeepLogData);
			//ToDo check for if there are error conditons and set flag to false
			Utils.printInConsole("maxDaysToKeepOldLogData : " + maxDaysToKeepOldLogData + "Date : "+ lastDateToKeepLogData + "deleteOldTables : "+ deleteOldLogs);
			deleteOldLogs = false;
		}

	}
	private void deleteOldLogs(LocalDate lastDateToKeepLogData) {

		int queryResult = visitorLogsRepository.deleteByDate(lastDateToKeepLogData );
		Utils.printInConsole("Delete Query Result : " + queryResult);
			
		
		List<VisitorLogs> Logs = (List<VisitorLogs>)visitorLogsRepository.selectLogsBeforeDate(lastDateToKeepLogData );
		if(Logs != null)
		{
			if(Logs.size() == 0) {
				Utils.printInConsole("No Records found");
			}
			for(int j=0;j<Logs.size();j++)
			{
				Utils.printInConsole("Date : "+ Logs.get(j).getDate() + "Result : "+ Logs.get(j).getResult());
			}
		}
		else {
			Utils.printInConsole("Logs null");
		}
	}

	private LocalDate getLastDateToKeepLogData(int daysToKeepOldLogData2) {
		LocalDate LastDate = LocalDate.now().minusDays(maxDaysToKeepOldTables);
		return LastDate;
	}

	@SuppressWarnings("unchecked")
	private void deleteOldLocationTable(LocalDate lastDateToKeepLocationTable) {

		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		String lastDate = lastDateToKeepLocationTable.format(formatters);
		List<String> tableNames = (List<String>)tableUtils.SelectTableNamesLike(getVisitorTableName(), lastDate );
		if(tableNames != null)
		{
			for(int j=0;j<tableNames.size();j++)
			{
				Utils.printInConsole("In Between chrono Table Names : " + tableNames.get(j) + "last date : "+ lastDateToKeepLocationTable.toString());
				tableUtils.dropLocationTable(tableNames.get(j));
			}
		}
	}

	private LocalDate getLastDateToKeepLocationTable(int maxDaysToKeepOldTables) {

		LocalDate LastDate = LocalDate.now().minusDays(maxDaysToKeepOldTables);

		return LastDate;
	}
	
	private String getVisitorTableName() {
		return getTableName("Visitor");
	}

	private String getTableName(String tableName) {
		String currentTableName = "";
		String profileName = tableName + "_Location";

		currentTableName = profileName + "_";

		return currentTableName;
	}
}
