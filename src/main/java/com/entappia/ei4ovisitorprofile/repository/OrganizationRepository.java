package com.entappia.ei4ovisitorprofile.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovisitorprofile.dbmodels.Organization;

@Repository
public interface OrganizationRepository extends CrudRepository<Organization, Integer> {

}
