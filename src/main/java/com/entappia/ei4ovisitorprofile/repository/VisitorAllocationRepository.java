package com.entappia.ei4ovisitorprofile.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovisitorprofile.dbmodels.VisitorAllocation;

@Repository
public interface VisitorAllocationRepository extends CrudRepository<VisitorAllocation, Integer> {

}
