package com.entappia.ei4ovisitorprofile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovisitorprofile.dbmodels.Notifications; 

@Repository
public interface NotificationsRepository extends CrudRepository<Notifications, Integer>{
	 
	@Query(value = "select * from notifications where type=:type and id>:id", nativeQuery = true)
	List<Notifications> findUnreadNotifications(@Param("id") long id, @Param("type") int type);
	
	
	@Query(value = "Select max(id) from notifications", nativeQuery = true)
	long getMaxNotificationId();
	
	@Query(value = "SELECT * FROM notifications where id < :id order by id desc limit :noOfRecords", nativeQuery = true)
	List<Notifications> getNotifications(@Param("id") long id, @Param ("noOfRecords") int noOfRecords );


}
