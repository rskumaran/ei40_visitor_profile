package com.entappia.ei4ovisitorprofile.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovisitorprofile.dbmodels.Department; 

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Integer> {
	List<Department> findAll();
	 
	Department findByDeptName(String deptName);
	 

}
