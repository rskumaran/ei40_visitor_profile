package com.entappia.ei4ovisitorprofile.repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovisitorprofile.dbmodels.VisitorLogs;
 

@Repository
public interface VisitorLogsRepository extends CrudRepository<VisitorLogs, Integer> {

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "DELETE FROM visitor_logs WHERE date < :date", nativeQuery = true)
	int deleteByDate(@Param("date") LocalDate date);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "select * FROM visitor_logs WHERE date < :date", nativeQuery = true)
	List<VisitorLogs> selectLogsBeforeDate(@Param("date") LocalDate date);

}
