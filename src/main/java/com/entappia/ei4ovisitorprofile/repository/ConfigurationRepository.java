package com.entappia.ei4ovisitorprofile.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovisitorprofile.dbmodels.Configuration;
 
@Repository
public interface ConfigurationRepository extends CrudRepository<Configuration, Integer> {

}
