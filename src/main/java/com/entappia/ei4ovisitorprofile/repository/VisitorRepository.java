package com.entappia.ei4ovisitorprofile.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovisitorprofile.dbmodels.Visitor;


@Repository
public interface VisitorRepository  extends CrudRepository<Visitor, Integer> {
	
	@Query(value = "select * from visitor where visitor_id=:visitorID", nativeQuery = true)
	Visitor getVisitorByVisitorId(@Param("visitorID") String empId);

}
