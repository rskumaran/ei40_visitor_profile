package com.entappia.ei4ovisitorprofile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4ovisitorprofile.dbmodels.Shift;

@Repository
public interface ShiftRepository extends CrudRepository<Shift, Integer> {
	
Shift findByShiftName(String shiftName);
	
List<Shift> findAll();

@Query(value = "SELECT * FROM shift where active = 1;", nativeQuery = true)
List<Shift> findActiveShifts();

}
