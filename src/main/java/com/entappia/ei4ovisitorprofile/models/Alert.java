package com.entappia.ei4ovisitorprofile.models;

import java.io.Serializable;

public class Alert implements Serializable {
	
	int sms;
	int mail;
 
	public int getSms() {
		return sms;
	}

	public void setSms(int sms) {
		this.sms = sms;
	}
 
	public int getMail() {
		return mail;
	}

	public void setMail(int mail) {
		this.mail = mail;
	}

}
