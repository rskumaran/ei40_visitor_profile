package com.entappia.ei4ovisitorprofile.utils;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entappia.ei4ovisitorprofile.constants.AppConstants.LogEventType;
import com.entappia.ei4ovisitorprofile.dbmodels.VisitorLogs;
import com.entappia.ei4ovisitorprofile.repository.VisitorLogsRepository;

@Service
public class LogEvents {

	@Autowired
	VisitorLogsRepository visitorLogsRepository;

	public synchronized void addLogs(LogEventType type, String major, String minor, String message) {
		if (visitorLogsRepository != null) {
			VisitorLogs logs = new VisitorLogs();
			logs.setDate(new Date());

			if (LogEventType.ERROR == type)
				logs.setType("Error");
			else
				logs.setType("Event");
			logs.setMajor(major);
			logs.setMinor(minor);
			logs.setResult(message);
			visitorLogsRepository.save(logs);
		}
	}

}
