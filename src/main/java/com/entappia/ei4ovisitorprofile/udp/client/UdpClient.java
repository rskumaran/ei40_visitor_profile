package com.entappia.ei4ovisitorprofile.udp.client;

public interface UdpClient {
	public void sendMessage(String message);
}
