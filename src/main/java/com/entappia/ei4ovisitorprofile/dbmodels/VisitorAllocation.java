package com.entappia.ei4ovisitorprofile.dbmodels;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4ovisitorprofile.constants.AppConstants;
import com.entappia.ei4ovisitorprofile.dbmodels.idclass.VisitorAllocationIdClass;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "VisitorAllocation")
@IdClass(VisitorAllocationIdClass.class)
public class VisitorAllocation implements Serializable {

	@Id 
	@Column(nullable = false, columnDefinition = "SMALLINT")
	int visitorId;
	
	@Id
	@Column(length = 12)
	String macId;
	
	@Column(length = 20)
	String status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date allocationStartTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date allocationEndTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date returnTime;
 

	public int getVisitorId() {
		return visitorId;
	}

	public void setVisitorId(int visitorId) {
		this.visitorId = visitorId;
	}

	public String getMacId() {
		return macId;
	}

	public void setMacId(String macId) {
		this.macId = macId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getAllocationStartTime() {
		return allocationStartTime;
	}

	public void setAllocationStartTime(Date allocationStartTime) {
		this.allocationStartTime = allocationStartTime;
	}

	public Date getAllocationEndTime() {
		return allocationEndTime;
	}

	public void setAllocationEndTime(Date allocationEndTime) {
		this.allocationEndTime = allocationEndTime;
	}

	public Date getReturnTime() {
		return returnTime;
	}

	public void setReturnTime(Date returnTime) {
		this.returnTime = returnTime;
	}

 

}
