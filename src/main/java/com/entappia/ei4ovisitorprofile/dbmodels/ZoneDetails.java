package com.entappia.ei4ovisitorprofile.dbmodels;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.entappia.ei4ovisitorprofile.constants.AppConstants;
import com.entappia.ei4ovisitorprofile.constants.AppConstants.ZoneType;
import com.entappia.ei4ovisitorprofile.converters.CoordinateConverter;
import com.entappia.ei4ovisitorprofile.converters.ListConverter;
import com.entappia.ei4ovisitorprofile.converters.WorkStationConverter;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "zoneDetails")
public class ZoneDetails {

	@Id
	@Column(nullable = false, columnDefinition = "SMALLINT")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	int id;

	@OneToOne
	@JoinColumn(name = "campusId", referencedColumnName = "campusId")
	CampusDetails campusDetails;
	
	@Column(length = 30)
	String name;

	@Column(length = 20)
	String type;

	@Convert(converter = CoordinateConverter.class)
	@Column(name = "coordinateList", columnDefinition = "json")
	List<HashMap<String, String>> coordinateList;

	@Convert(converter = ListConverter.class)
	@Column(name = "gridList", columnDefinition = "json")
	List<String> gridList;

	@Column(length = 20)
	String accessControl;
	
	@Column(length = 128)
	String description;
	

	@Column(length = 30)
	String area;

	@OneToOne
	@JoinColumn(name = "zoneClassification", referencedColumnName = "id")
	ZoneClassification zoneClassification;

	@Column(nullable = false, columnDefinition = "SMALLINT")
	int capacity;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone = AppConstants.timeZone)
	Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone = AppConstants.timeZone)
	Date modifiedDate;

	boolean active;

	@Enumerated
	@Column(name = "zoneType", length = 2)
	ZoneType zoneType;

	@Column(length = 10)
	String geometryType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public CampusDetails getCampusDetails() {
		return campusDetails;
	}

	public void setCampusDetails(CampusDetails campusDetails) {
		this.campusDetails = campusDetails;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<HashMap<String, String>> getCoordinateList() {
		return coordinateList;
	}

	public void setCoordinateList(List<HashMap<String, String>> coordinateList) {
		this.coordinateList = coordinateList;
	}

	public List<String> getGridList() {
		return gridList;
	}

	public void setGridList(List<String> gridList) {
		this.gridList = gridList;
	}
 
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAccessControl() {
		return accessControl;
	}

	public void setAccessControl(String accessControl) {
		this.accessControl = accessControl;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public ZoneType getZoneType() {
		return zoneType;
	}

	public void setZoneType(ZoneType zoneType) {
		this.zoneType = zoneType;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public ZoneClassification getZoneClassification() {
		return zoneClassification;
	}

	public void setZoneClassification(ZoneClassification zoneClassification) {
		this.zoneClassification = zoneClassification;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getGeometryType() {
		return geometryType;
	}

	public void setGeometryType(String geometryType) {
		this.geometryType = geometryType;
	}

}
